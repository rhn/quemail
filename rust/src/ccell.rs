/*
 * Copyright (C) 2018 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

use std::cell::UnsafeCell;

pub struct CachedCell<T> (UnsafeCell<Option<T>>);

/// A RefCell that can only be written once. Allows for safe returning of references to contents
impl<T> CachedCell<T> {
    pub fn new() -> CachedCell<T> {
        CachedCell(UnsafeCell::new(None))
    }
    pub fn set(&self, val: T) -> &T {
        if let Some(_) = self.get() {
            panic!("Setting data after it was set");
        }
        unsafe {
            *self.0.get() = Some(val);
        }
        self.get().unwrap()
    }
    pub fn get(&self) -> Option<&T> {
        let value = unsafe {
            &*self.0.get()
        };
        match value {
            &None => None,
            &Some(ref d) => Some(d),
        }
    }
}
