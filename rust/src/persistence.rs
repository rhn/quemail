extern crate serde_yaml;
extern crate xdg;

use std::io;
use std::io::{ BufReader, BufWriter };
use std::fs;
use std::path::PathBuf;
use std::vec::Vec;


#[derive(Debug)]
pub enum PersistenceError {
    Io(PathBuf, io::Error),
    BaseDirectories(xdg::BaseDirectoriesError),
    Serde(PathBuf, serde_yaml::Error),
}

impl From<xdg::BaseDirectoriesError> for PersistenceError {
    fn from(e: xdg::BaseDirectoriesError) -> Self {
        PersistenceError::BaseDirectories(e)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Query {
    pub name: String,
    pub query: String,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Bookmarks {
    #[serde(default)]
    pub queries: Vec<Query>,
}

fn get_data_file() -> Result<PathBuf, PersistenceError> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("quemail")?;
    xdg_dirs.place_data_file("bookmarks")
        .map_err(|e| {
            PersistenceError::Io(xdg_dirs.get_data_home(), e)
        })
}    

impl Bookmarks {
    pub fn create_or_load() -> Result<Bookmarks, PersistenceError> {
        let data_file_path = get_data_file()?;

        let from_io = |ioerr| {
            PersistenceError::Io(data_file_path.clone(), ioerr)
        };
        let from_yaml = |ymlerr| {
            PersistenceError::Serde(data_file_path.clone(), ymlerr)
        };

        match fs::OpenOptions::new()
                .write(true)
                .create_new(true)
                .open(&data_file_path) {
            Ok(f) => {
                serde_yaml::to_writer(BufWriter::new(f), &Bookmarks::default())
                    .map_err(from_yaml)?
            },
            Err(e) => {
                if e.kind() != io::ErrorKind::AlreadyExists {
                    return Err(from_io(e))
                }
            },
        };
            
        Ok(
            serde_yaml::from_reader(
                BufReader::new(
                    fs::OpenOptions::new()
                        .read(true)
                        .open(&data_file_path)
                        .map_err(from_io)?
                )
            ).map_err(from_yaml)?
        )
    }
    
    pub fn save(&self) -> Result<(), PersistenceError> {
        let data_file_path = get_data_file()?;
        
        let from_io = |ioerr| {
            PersistenceError::Io(data_file_path.clone(), ioerr)
        };
        let from_yaml = |ymlerr| {
            PersistenceError::Serde(data_file_path.clone(), ymlerr)
        };
        // maybe TODO: check if the timestamp is unchanged
        fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .open(&data_file_path).map_err(from_io)
            .map(BufWriter::new)
            .and_then(|w| {
                serde_yaml::to_writer(w, self).map_err(from_yaml)
            })
    }
}
