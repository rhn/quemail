/*
 * Copyright (C) 2018, 2019 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

/*! Bookmark hadling and using them */

use ::db;
use ::persistence;


#[derive(Debug, Clone)]
pub enum Bookmark {
    Tag(String),
    Query(persistence::Query),
    Directory(String),
    Uncategorized,
    UnreadUncategorized,
}

impl Bookmark {
    pub fn get_type_num(&self) -> u32 {
        match self {
            Bookmark::Tag(_) => 0,
            Bookmark::Query(_) => 1,
            Bookmark::Directory(_) => 2,
            Bookmark::Uncategorized => 3,
            Bookmark::UnreadUncategorized => 3,
        }
    }
    
    pub fn from_tag_name(name: String) -> Option<Bookmark> {
        match name.as_str() {
            "signed" => None,
            "attachment" => None,
            "encrypted" => None,
            other => Some(Bookmark::Tag(other.into())),
        }
    }
    
    pub fn to_name(&self) -> &str {
        match self {
            Bookmark::Tag(ref name) => match name.as_str() {
                "inbox" => "Waiting",
                name => name,
            },
            Bookmark::Query(persistence::Query{ref name, query: _}) => name,
            Bookmark::Directory(ref name) => name,
            Bookmark::Uncategorized => "uncategorized",
            Bookmark::UnreadUncategorized => "unc_unread",
        }
    }
}

pub struct Collection<'a>(pub &'a [Bookmark]);

impl<'a> Collection<'a> {
    fn get_uncategorized_query(&self) -> String {
        self.0.iter().map(|b| match b {
            Bookmark::Query(persistence::Query{name: _, ref query}) => {
                format!("not ({})", query)
            },
            _ => panic!("not a query among queries")
        }).collect::<Vec<_>>()
            .as_slice()
            .join(" and ")
    }
    pub fn to_query(&self, bookmark: Bookmark) -> db::Query {
        use self::Bookmark::*;
        db::Query(match bookmark {
            Tag(ref name) => format!("tag:{}", name),
            Query(persistence::Query{name: _, ref query}) => query.clone(),
            Directory(ref name) => name.clone(),
            Uncategorized => self.get_uncategorized_query(),
            UnreadUncategorized => format!("({}) and tag:unread", self.get_uncategorized_query()),
        })
    }
}
