/*
 * Copyright (C) 2018, 2019 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

/*! NotMuch database access procedures */

use std::env;
use std::sync::PoisonError;
use std::fmt;

use notmuch;

use data;
use utils;


#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Query(pub String);

/// Database access error
#[derive(Debug)]
pub enum Error {
    TextError(&'static str),
    Poisoned(String),
    MultipleHits(String),
    Notmuch(notmuch::Error),
    MaildirMissing(env::VarError),
}

impl From<notmuch::Error> for Error {
    fn from(e: notmuch::Error) -> Self {
        Error::Notmuch(e)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DB access: ")?;
        match self {
            Error::TextError(s) => write!(f, "{}", s),
            Error::Poisoned(s) => write!(f, "{}", s),
            Error::MultipleHits(s) => write!(f, "Multiple messages with id: {}", s),
            Error::Notmuch(e) => write!(f, "{}", e),
            Error::MaildirMissing(e) => write!(f, "MAILDIR env variable not readable: {}", e),
        }
    }
}


type Guard<'lock, T> = std::sync::MutexGuard<'lock, T>;

/// Locked struct access
#[derive(Debug)]
pub enum LockError<'db, L> {
    LockPoisoned(PoisonError<Guard<'db, L>>),
    Error(Error),
}

impl<'db, L> From<PoisonError<Guard<'db, L>>> for LockError<'db, L> {
    fn from(e: PoisonError<Guard<'db, L>>) -> Self {
        LockError::LockPoisoned(e)
    }
}

impl<'db, L> From<Error> for LockError<'db, L> {
    fn from(e: Error) -> Self {
        LockError::Error(e)
    }
}

impl<'db, L> fmt::Display for LockError<'db, L> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            LockError::Error(e) => write!(f, "{}", e),
            LockError::LockPoisoned(e) => write!(f, "{}", e),
        }
    }
}

pub fn open(path: String, mode: notmuch::DatabaseMode)
    -> Result<notmuch::Database, Error>
{
    notmuch::Database::open(&path, mode)
        .map_err(Error::from)
}

pub fn open_or_create(path: &String) -> Result<notmuch::Database, Error> {
    notmuch::Database::open(path, notmuch::DatabaseMode::ReadWrite)
        .or_else(|e| notmuch::Database::create(path))
        .map_err(Error::from)
}

use std::io;
use std::fs::{self, DirEntry};
use std::path::Path;

// From https://doc.rust-lang.org/std/fs/fn.read_dir.html
fn visit_dirs(dir: &Path, cb: &mut dyn FnMut(DirEntry)) -> io::Result<()> {
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                visit_dirs(&path, cb)?;
            } else {
                cb(entry);
            }
        }
    }
    Ok(())
}

#[derive(Debug)]
pub enum IndexError {
    IO(io::Error),
    NM(notmuch::Error),
}

/// Will hold some kind of changes...
pub struct Changes {}

pub fn with_new_messages(db: notmuch::Database, path: &Path)
    -> (notmuch::Database, Option<IndexError>, Option<Changes>)
{
    let mut files = Vec::new();
    let r
        = visit_dirs(
            path,
            &mut |de| {
                (&mut files).push(de)
            }
        )
        .map_err(IndexError::IO)
        .and_then(|()| {
            for file in files {
                let path = file.path();
                if let None = db.find_message_by_filename(&path).map_err(IndexError::NM)? {
                    use notmuch::Error::NotmuchError;
                    use notmuch::Status;
                    match db.index_file(&path, None) {
                        Err(NotmuchError(Status::FileNotEmail)) => Ok(()),
                        Err(NotmuchError(Status::DuplicateMessageID)) => Ok(()),
                        Err(e) => Err(IndexError::NM(e)),
                        Ok(_m) => Ok(()),
                    }?;
                }
            }
            Ok(())
        });

    let e = match r {
        Ok(()) => None,
        Err(e) => Some(e),
    };
    (db, e, Some(Changes{}))
}

pub fn process_message<T, E: From<Error>>(
    db: &notmuch::Database,
    msg_id: &str, f: &dyn Fn(notmuch::Message<notmuch::Query>) -> Result<T, E>
) -> Result<T, E> {
    let query = db.create_query(&format!("id:{}", msg_id)).map_err(Error::Notmuch)?;
    if query.count_messages().map_err(Error::Notmuch)? > 1 {
        return Err(Error::MultipleHits(msg_id.into()).into());
    }
    // Do not drop Messages before the Message is dropped! Notmuch will crash
    let mut messages = query.search_messages().map_err(Error::Notmuch)?;
    let message = messages.next().ok_or(Error::TextError("No messages found"))?;
    f(message)
}


pub fn fetch_record(db: &notmuch::Database, msg_id: &str)
        -> Result<data::MessageRecord, Error> {
    process_message(db, msg_id, &|message: notmuch::Message<notmuch::Query>| {
        let unread = message.tags().into_iter().any(|t| t == "unread");
        let inbox = message.tags().into_iter().any(|t| t == "inbox");
        Ok(data::MessageRecord { unread: unread, inbox: inbox })
    })
}

use inotify::{
    WatchMask,
    Inotify,
};

use std::sync;
use std::sync::{ Arc, Mutex };
use std::sync::atomic::{ AtomicBool, Ordering };
use std::thread;

/// A crappy IMAP directory recurser.
/// Should be fine as long as stuff arrives in INBOX only.
/// Solves the issue of recursing into .notmuch
fn recurse_imap(path: &str) -> Vec<String> {
    vec![
        path.to_owned() + "/INBOX",
        path.to_owned() + "/Drafts",
        path.to_owned() + "/Sent",
        path.to_owned() + "/Trash",
    ]
}

/// Scans for changes in an IMAP directory.
/// Only watches for when a file was finished being written to (like create) ATM.
/// Don't create many instances. Number of watched dirs is limited.
pub struct ChangeScanner {
    thread: thread::JoinHandle<()>,
    dirty: Arc<AtomicBool>,
    callback: Arc<Mutex<Option<Box<dyn FnMut() + Send>>>>,
}

use std::ops::DerefMut;

impl ChangeScanner {
    pub fn new(path: String) -> ChangeScanner {
        // TODO: warn when scanner flopped
        // TODO: name this thread after watched dir
        let dirty = Arc::new(AtomicBool::new(true));
        let dirty_in = dirty.clone();
        let callback_cont = Arc::new(Mutex::new(None));
        let callback_in = callback_cont.clone();
        let watcher = thread::spawn(move || {
            Inotify::init().map(|mut inotify| {
                for sub in recurse_imap(&path) {
                    inotify.add_watch(&sub, WatchMask::CLOSE_WRITE)
                        .map(|_| {}) // ignore watch handles
                        .unwrap_or_else(|e| eprintln!("Can't watch dir {} because {}", sub, e));
                }

                let mut buffer = [0; 4096];
                loop {
                    inotify.read_events_blocking(&mut buffer)
                        .map(|events| {
                            for _event in events {
                                let old = dirty_in.swap(true, Ordering::SeqCst);
                                // TODO: ignore .files
                                if old == false {
                                    let mut cb_guard: sync::MutexGuard<Option<Box<dyn FnMut() + Send>>>
                                        = utils::get_guard(callback_in.lock());
                                    if let Some(ref mut cb) = cb_guard.deref_mut() {
                                        let cb = Box::as_mut(cb);
                                        cb();
                                    }
                                }
                            }
                        })
                        .unwrap_or_else(|e| eprintln!("Failed to read inotify events because {}", e));
                }
            }).unwrap_or_else(|e| eprintln!("Failed to start inotify because {}", e));
        });
        ChangeScanner {
            thread: watcher,
            dirty,
            callback: callback_cont,
        }
    }

    pub fn reset(&self) {
        self.dirty.swap(false, Ordering::SeqCst);
    }

    pub fn set_callback(&self, cb: Option<Box<dyn FnMut() + Send>>) {
        let mut guard = utils::get_guard(self.callback.lock());
        *guard = cb;
    }

    pub fn get_dirty(&self) -> bool {
        self.dirty.load(Ordering::SeqCst)
    }
}



pub enum ApplyResult<T> {
    ErrOpen(notmuch::Error),
    Success(T),
    ErrClose(T, notmuch::Error),
}

impl<T, E: fmt::Display> ApplyResult<Result<T, E>> {
    pub fn warn_ok(self) -> Option<T> {
        let err_to_str = |e| format!("Failed to query database: {}", e);
        let result = match self {
            ApplyResult::ErrOpen(e) => Err(
                format!("Failed to open the database: {}", e)
            ),
            ApplyResult::ErrClose(v, e) => {
                eprintln!("Failed to close the database: {}", e);
                v.map_err(err_to_str)
            },
            ApplyResult::Success(v) => v.map_err(err_to_str),
        };
        match result {
            Ok(value) => Some(value),
            Err(e) => {
                eprintln!("Failed to count messages: {}", e);
                None
            }
        }
    }
}
