/*! Random stuff that's used across modules.
 */

use std::sync;
use std::vec::Vec;


/// Allows to use "?" without creating an additional function by allowing a catching action.
///
/// ```
/// let foo = catch_try!(
///     {
///         let bar = mutex.lock()?;
///         Ok(bar.foo.clone())
///     },
///     |e| {
///         eprintln("Fail: {}", e);
///         Foo::default()
///     }
/// );
#[macro_export]
macro_rules! catch_try {
    ($e:expr, $handler:expr) => {{
        let foo = || {
            $e
        };
        foo().unwrap_or_else($handler)
    }}
}

#[macro_export]
macro_rules! catch_try_mut {
    ($e:expr, $handler:expr) => {{
        let mut foo = || {
            $e
        };
        foo().unwrap_or_else($handler)
    }}
}

pub fn try_or_else<F, H, T, E>(mut f: F, handler: H) -> T
    where F: FnMut() -> Result<T, E>,
        H: FnMut(E) -> T
{
    f().unwrap_or_else(handler)
}

pub fn try_or_else_none<F, H, T, E>(mut f: F, mut handler: H) -> Option<T>
    where F: FnMut() -> Result<T, E>,
        H: FnMut(E) -> ()
{
    Some(f())
        .transpose()
        .unwrap_or_else(|e|
        {
            handler(e);
            None
        }
    )
}

/// Special version of Index that doesn't allow items to outlive the owner
pub trait Index<'a> {
    type Output: ?Sized;
    fn index(&self, i: usize) -> &'a Self::Output;
}

impl<'a, T> Index<'a> for &'a Vec<T> {
    type Output = T;
    fn index(&self, i: usize) -> &'a Self::Output {
        &self[i]
    }
}

pub trait ExactSize {
    fn len(&self) -> usize;
}

// Cannot define ExactSizeIterator for Vec, because neither is defined here
impl<T> ExactSize for &Vec<T> {
    fn len(&self) -> usize {
        Vec::len(self)
    }
}

// TODO: maybe throw away chaining and just accept the subslices in a vector?
/// Allows for indexing separate items as if they were consecutive.
/// The second item is always a slive. Can be chained when using CombinedSlice as the first element.
pub struct CombinedSlice<'a, T, I: ExactSize + Index<'a, Output=T>> {
    s0: I, // Box would make declarations a little clearer
    s1: &'a [T],
}

impl<'a, T, I: ExactSize + Index<'a, Output=T>> CombinedSlice<'a, T, I> {
    pub fn new(s0: I, s1: &'a [T]) -> Self {
        CombinedSlice { s0, s1 }
    }

    pub fn get(&self, i: usize) -> Option<&'a T> {
        Some(self.index(i))
    }
}

impl<'a, T, I: ExactSize + Index<'a, Output=T>> ExactSize for CombinedSlice<'a, T, I> {
    fn len(&self) -> usize {
        self.s0.len() + self.s1.len()
    }
}

impl<'a, T, I: ExactSize + Index<'a, Output=T>> Index<'a>
        for CombinedSlice<'a, T, I> {
    type Output = T;
    fn index(&self, i: usize) -> &'a T {
        if i < self.s0.len() {
            self.s0.index(i)
        } else {
            &self.s1[i - self.s0.len()]
        }
    }
}

/// Ignores the PoisonError and returns the lock guard.
/// Mutex.lock() will return Err even if it succeeded
/// to indicate another thread's failure. We don't care.
pub fn get_guard<'a, T>(r: sync::LockResult<sync::MutexGuard<'a, T>>)
    -> sync::MutexGuard<'a, T>
{
    match r {
        Ok(guard) => guard,
        Err(e) => {
            eprintln!("Ignoring locking problem: {}", e);
            e.into_inner()
        }
    }
}

mod test {
    #[test]
    fn test_index_second() {
        let a = vec![1, 2, 3];
        let b = vec![4, 5, 6];
        assert_eq!(5, CombinedSlice::new(&a, b.as_slice()).get(4).unwrap().clone());
    }
    
    #[test]
    fn test_index_first_last() {
        let a = vec![1, 2, 3];
        let b = vec![4, 5, 6];
        assert_eq!(3, CombinedSlice::new(&a, b.as_slice()).get(2).unwrap().clone());
    }
    
    #[test]
    fn test_index_last_first() {
        let a = vec![1, 2, 3];
        let b = vec![4, 5, 6];
        assert_eq!(4, CombinedSlice::new(&a, b.as_slice()).get(3).unwrap().clone());
    }
    
    #[test]
    fn test_index_firstempty() {
        let a = Vec::new();
        let b = vec![4, 5, 6];
        assert_eq!(5, CombinedSlice::new(&a, b.as_slice()).get(1).unwrap().clone());
    }
    
    #[test]
    fn test_index_secondempty() {
        let a = vec![1, 2, 3];
        let b = Vec::new();
        assert_eq!(2, CombinedSlice::new(&a, b.as_slice()).get(1).unwrap().clone());
    }
}
