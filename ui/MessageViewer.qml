/*
 * Copyright (C) 2014 Sergei Shilovsky <sshilovsky@gmail.com>
 * Copyright (C) 2018 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.10
import Quemail 1.0

Item {
    id: root
    property alias messagePath: contents.filename
    property alias msgId: contents.msgId
    property Database database
    property MessageRecord message: {
        if (database) {
            database.getMessage(contents.msgId)
        } else {
            null
        }
    }

    onMsgIdChanged: {
        if (database) {
            message = database.getMessage(contents.msgId)
        }
    }

    onDatabaseChanged: {
        if (database) {
            message = database.getMessage(contents.msgId)
        }
    }

    // TODO: ondatabasechanged: need to update message read status

    onMessagePathChanged: {
        body.flickableItem.contentY = 0;
    }

    // MessageLoader reads the message data from a file, and has no access to the tag database
    MessageLoader {
        id: contents
    }

    RowLayout {
        anchors.fill: parent
        ColumnLayout {
            RowLayout {
                GridLayout {
                    id: nice_headers
                    rows: 4
                    columns: 2
                    Layout.fillWidth: true
                    visible: !want_raw_headers.checked
                    Label {
                        text: qsTr("From:")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        Layout.fillWidth: false
                    }
                    SelectableLabel {
                        id: from
                        text: contents.sender
                        Layout.fillHeight: false
                        Layout.fillWidth: true
                    }
                    Label {
                        text: qsTr("To:")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }
                    SelectableLabel {
                        id: to
                        text: contents.recipients
                        Layout.fillHeight: false
                        Layout.fillWidth: true
                    }
                    Label {
                        text: qsTr("Date:")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }
                    SelectableLabel {
                        id: date
                        text: contents.date
                        Layout.fillHeight: false
                        Layout.fillWidth: true
                    }
                    Label {
                        text: qsTr("Subject:")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }
                    SelectableLabel {
                        id: subject
                        text: contents.subject
                        Layout.fillHeight: false
                        Layout.fillWidth: true
                    }
                }

                // TODO: load lazily
                TextArea {
                    id: raw
                    visible: want_raw_headers.checked
                    readOnly: true
                    text: contents.rawHeaders
                    Layout.fillWidth: true

                    flickableItem.boundsBehavior: Flickable.StopAtBounds
                }

                CheckBox {
                    id: want_raw_headers
                    text: qsTr("Raw headers")
                    Layout.alignment: Qt.AlignRight | Qt.AlignTop
                    Layout.fillHeight: false
                }
            }

            TextArea {
                id: body
                readOnly: true
                text: contents.body
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }

        ColumnLayout {
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Button {
                text: "Act later (-unread)"
                visible: message && message.unread && message.inbox
                onClicked: database.setRead(contents.msgId)
            }

            Button {
                text: "Done (-unread, -inbox)"
                visible: message && message.inbox
                onClicked: database.setDone(contents.msgId)
            }
        }
    }
}
