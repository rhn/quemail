/*
 * Copyright (C) 2014 Sergei Shilovsky <sshilovsky@gmail.com>
 * Copyright (C) 2018 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import Quemail 1.0
import org.kde.kirigami 2.4 as Kirigami

ColumnLayout {
    id: root
    property alias queryString: ti.text
    property bool valid: true
    property Bookmarks bookmarks

    ColumnLayout {
        TextField {
            id: ti
            textColor: {
                if (valid) {
                    Kirigami.Theme.textColor
                } else {
                    Kirigami.Theme.negativeTextColor
                }
            }

            placeholderText: "query"
            Layout.fillWidth: true
        }
    }
}
