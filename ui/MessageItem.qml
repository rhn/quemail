/*
 * Copyright (C) 2014 Sergei Shilovsky <sshilovsky@gmail.com>
 * Copyright (C) 2018 rhn <gihu.rhn@porcupinefactory.org>
 *
 * SPDX-License-Identifier: GPL-3+
 */

import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.3
import Quemail 1.0

Item {
    id: root

    // TODO: eliminate redundant calls to getMessage

    property Messages model;
    property MessageMeta message: {
        if (styleData.row >= 0 && styleData.row < model.rowCount()) {
            model.getMessage(styleData.row)
        } else {
            null
        }
    }

    // workaround: the model doesn't change but switching queries should trigger the recalculation of properties on reused MessageItems
    property string query: model.query;

    onQueryChanged: {
        if (styleData.row >= 0 && styleData.row < model.rowCount()) {
            message = model.getMessage(styleData.row)
        }
    }

    // TODO: replace with a unique stable model index
    property string filename: styleData.value ? styleData.value : ""

    onFilenameChanged: {
        if (styleData.row >= 0 && styleData.row < model.rowCount()) {
            message = model.getMessage(styleData.row)
        }
    }

    RowLayout {
        anchors.leftMargin: message ? authors.height * message.depth : 0
        anchors.fill: root
        Layout.fillWidth: true

        Label {
            id: authors
            text: message ? message.peers : ""
            font.bold: message ? message.unread : false
            elide: Text.ElideRight
            Layout.fillWidth: true
        }
    }
}
