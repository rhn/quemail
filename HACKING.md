HACKING
=======

Tips for hacking on *Quemail*, chiefly here to prevent forgetting.

Dependencies
------------

Look in the `.gitlab-ci.yml` file.

Note that rq-bindgen is needed.

Running from source
-------------------

After make, there's no need to do make install. Instead:

```
MAILDIR=foo QML_DATA_DIR=./quemail/ui/ ./quemail-build/quemail
```

Profiling
---------

Install *prof* and *hotspot*.

Run quemail, and run hostpot, then select the option to attach to a PID.
